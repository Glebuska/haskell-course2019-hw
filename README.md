# HPython

Интерпретатор Python 3 на языке Haskell.

# Мотивация

Для ознакомления с загадочным миром функционального программирования было решено написать небольшой интерпретатор Python 3 на языке Haskell.
 
# Прогресс:
* [x] Лексер
* [x] Парсер
* [x] Простые типы данных: `int`, `list`, `float`, `string`, `bool` 
* [x] Унарные и бинарные операции
* [x] Конкатенация списков 
* [x] Двумерный синтаксис(отступы важны)
* [x] Конкатенация списков и строк при помощи `+`
* [x] Поддержка локальных и глобальных переменных
* [x] Циклы: `for` и `while`
* [x] Классы, методы, функции, вложенные классы и функции
* [x] [Динамическое добавление](http://codeblog.dhananjaynene.com/2010/01/dynamically-adding-methods-with-metaprogramming-ruby-and-python/) методов
* [x] Условная конструкция с `if` и `else`
* [x] Тернарный `if`
* [x] Лямбда функции 
* [ ]  Простой REPL
* [ ]  Деление 
* [ ]  Словари
* [ ]  Вывод типов с помощью ключевого слово type()
* [ ]  Обработка ошибок с помощью конструкции try 

# Примеры

Примеры вы можете [увидеть тут](https://gitlab.com/Glebuska/haskell-course2019-hw/-/blob/master/python/app/Main.hs)

# Запуск

1.  Установите [Stack](https://github.com/commercialhaskell/stack)
2.  Скачайте проект
3.  Запустите его `stack build && stack run

# Используемые материалы
*  [Hoogle](https://hoogle.haskell.org/)
*  [Python documentation](https://docs.python.org/3/)
*  [Megaparsec tutorial](https://markkarpov.com/tutorial/megaparsec.html)